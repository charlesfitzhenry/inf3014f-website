﻿<%@ Page Title="Product Details" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductDetails.aspx.cs" Inherits="StudentVibes.ProductDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <!-- Include jQuery. -->
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>

    <!-- Include Cloud Zoom CSS. -->
    <link rel="stylesheet" type="text/css" href="../cloudzoom/cloudzoom.css">

    <!-- Include Cloud Zoom script. -->
    <script type="text/javascript" src="../cloudzoom/cloudzoom.js"></script>

    <!-- Call quick start function. -->
    <script type="text/javascript">
        CloudZoom.quickStart();
    </script>
           <script>
               $(document).ready(function () {
                   /*! Fades in page on load */
                   $('body').css('display', 'none');
                   $('body').fadeIn(800);
               });
        </script> 

    <asp:FormView ID="productDetail" runat="server"
        ItemType="StudentVibes.Models.Product" SelectMethod="GetProduct"
        RenderOuterTable="false">


        <ItemTemplate>
            <div>
                <h1><%#:Item.ProductName %></h1>
                <h4 style="color: GrayText"><% //Item.Manufacturer %> Author / Manufacturer</h4>
            </div>
            <br />
            <div class="row">

                <div class="col-sm-6 col-lg-4">





                     <!--Main Image-->
                    <div class="text-center">
                        <img style="width: 30%; min-width: 300px;" class="cloudzoom" alt="Cloud Zoom small image" id="zoom1" src="/Catalog/Images/Thumbs/<%#:Item.ImagePath %>" title="Cloud Zoom has many configuration options to match the look and feel of your website" data-cloudzoom="
                     zoomImage:&quot;/Catalog/Images/Thumbs/<%#:Item.ImagePath %>&quot;,
                     zoomSizeMode: &quot;image&quot;,
                     tintColor:&quot;#000&quot;,
                     tintOpacity:0.25,
                     captionPosition:&quot;bottom&quot;,
                     maxMagnification:4,
                     autoInside:750
                     ">
                    </div>

                    <span class="gen-spacer"></span>
                    <br />

                    <!--Thumbnails-->
                    <div class="text-center">
                        <a href="/Catalog/Images/Thumbs/<%#:Item.ImagePath %>" class="thumb-link">
                            <img class="cloudzoom-gallery" width="64" src="/Catalog/Images/Thumbs/<%#:Item.ImagePath %>" title="Cloud Zoom has many configuration options to match the look and feel of your website" alt="Cloud Zoom thumb image" data-cloudzoom="
                         useZoom:&quot;#zoom1&quot;,
                         image:&quot;/Catalog/Images/Thumbs/<%#:Item.ImagePath %>&quot;,
                         zoomImage:&quot;/Catalog/Images/Thumbs/<%#:Item.ImagePath %>&quot;">
                        </a>

                        <a href="http://www.picturesnew.com/media/images/images-background.jpg" class="thumb-link">
                            <img class="cloudzoom-gallery" width="64" src="http://www.picturesnew.com/media/images/images-background.jpg" alt="Cloud Zoom thumb image" title="Works great with iPad and other touch-enabled devices" data-cloudzoom="
                         useZoom:&quot;#zoom1&quot;,
                         image:&quot;http://www.picturesnew.com/media/images/images-background.jpg&quot;,
                         zoomImage:&quot;http://www.picturesnew.com/media/images/images-background.jpg&quot;">
                        </a>

                        <a href="/sites/starplugins/images/jetzoom/large/image3.jpg" class="thumb-link">
                            <img class="cloudzoom-gallery" width="64" src="/sites/starplugins/images/jetzoom/small/image3.jpg" alt="Cloud Zoom thumb image" title="Regular free updates and new features with technical support" data-cloudzoom="
                         useZoom:&quot;#zoom1&quot;,
                         image:&quot;/sites/starplugins/images/jetzoom/small/image3.jpg&quot;
                         ">
                        </a>
                    </div>


                </div>


                <div class="col-sm-6 col-lg-4">

                    <b>Description:</b><br />
                    <%#:Item.Description %>
                    <br />
                    <span>
                        <b>Price:</b>&nbsp;<%#: String.Format("{0:c}", Item.UnitPrice) %>
                    </span>
                    <br />

                    <span>
                        <b>Product Number:</b>&nbsp;<%#:Item.ProductID %>
                    </span>
                    <br />
                    <a href="/AddToCart.aspx?productID=<%#:Item.ProductID %>" class="btn btn-primary" role="button"><i class="glyphicon glyphicon-plus"></i>Add to Cart</a>




                </div>
            </div>
        </ItemTemplate>
    </asp:FormView>
</asp:Content>
