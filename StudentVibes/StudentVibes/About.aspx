﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="StudentVibes.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <meta name="description" content="Would you like to know more about Student Vibes? All our information
        is explained below.">
    <meta name="author" content="Student Vibes">
    <h3>Your application description page.</h3>
    <p>Use this area to provide additional information.</p>
    <script>
        $(document).ready(function () {
            /*! Fades in page on load */
            $('body').css('display', 'none');
            $('body').fadeIn(800);
        });
        </script>
</asp:Content>