﻿<%@ Page Title="Products" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductList.aspx.cs" Inherits="StudentVibes.ProductList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script>
        $(document).ready(function () {
            /*! Fades in page on load */
            $('body').css('display', 'none');
            $('body').fadeIn(800);
        });
    </script>

    <section>

        <hgroup>
            <h2><%: Page.Title %></h2>
        </hgroup>

        <asp:ListView ID="productList" runat="server"
            DataKeyNames="ProductID"
            ItemType="StudentVibes.Models.Product" SelectMethod="GetProducts">

            <LayoutTemplate>
                <div class="row">

                    <asp:PlaceHolder runat="server" ID="itemPlaceholder" />

                </div>
            </LayoutTemplate>

            <ItemTemplate>
                <div class="col-sm-4 col-md-3 col-lg-3">
                    <div class="thumbnail">
                        <a style="text-decoration: NONE" href=" <%#: GetRouteUrl("ProductByNameRoute", new {productName = Item.ProductName}) %>">
                            <img src="/Catalog/Images/Thumbs/<%#:Item.ImagePath %>" alt="<%#: Item.ProductName %>">
                            <div class="caption">
                                <h3 style="margin-top: 0px; margin-bottom: 3px;"><%#: Item.ProductName %></h3>
                        </a>

                        <h5 style="margin-top: 0px; color: GrayText;"><%#:Item.Brand %></h5>


                        <p style="height: 70px;">This is where the item description will be displayed.</p>
                        <p><a href="/AddToCart.aspx?productID=<%#:Item.ProductID %>" class="btn btn-primary" role="button"><i class="glyphicon glyphicon-plus"></i>Add to Cart</a> </p>
                    </div>
                </div>
                </div>
            </ItemTemplate>

            <EmptyDataTemplate>
            </EmptyDataTemplate>
        </asp:ListView>

    </section>
</asp:Content>
