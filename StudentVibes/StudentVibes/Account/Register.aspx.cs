﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using StudentVibes.Models;
using System.Data;
using System.Data.SqlClient;

namespace StudentVibes.Account
{
    public partial class Register : Page
    {
        protected void CreateUser_Click(object sender, EventArgs e)
        {
            var manager = new UserManager();
            var user = new ApplicationUser() { UserName = UserName.Text };
            IdentityResult result = manager.Create(user, Password.Text);
            if (result.Succeeded)
            {
                Random rnd = new Random();
                string id = "";

                for (int i = 0; i < 10; i++)
                {
                    id += rnd.Next(0, 10);
                }

                SqlConnection connection = new SqlConnection("Data Source=(LocalDB)\\v11.0;AttachDbFilename=|DataDirectory|\\StudentVibes.mdf;Integrated Security=True");
                connection.Open();
                using (SqlCommand cmd = new SqlCommand("INSERT INTO Customers (Password, UserName, FullName, CustID, Location) VALUES (@Password, @UserName, @FullName, @CustID, @Location)", connection))
                {
                    cmd.Parameters.AddWithValue("@Password", user.PasswordHash);
                    cmd.Parameters.AddWithValue("@UserName", UserName.Text);
                    cmd.Parameters.AddWithValue("@FullName", FullName.Text);
                    cmd.Parameters.AddWithValue("@CustID", id);
                    cmd.Parameters.AddWithValue("@Location", Location.Text);
                    cmd.ExecuteNonQuery();
                }
                connection.Close();

                IdentityHelper.SignIn(manager, user, isPersistent: false);

                using (StudentVibes.Logic.ShoppingCartActions usersShoppingCart = new StudentVibes.Logic.ShoppingCartActions())

                { String cartId = usersShoppingCart.GetCartId(); usersShoppingCart.MigrateCart(cartId, user.Id); }


                IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
            }
            else
            {
                ErrorMessage.Text = result.Errors.FirstOrDefault();
            }
        }
    }
}