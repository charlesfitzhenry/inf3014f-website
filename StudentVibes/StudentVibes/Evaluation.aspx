﻿<%@ Page Title="Sell an Item" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" 
    CodeBehind="Evaluation.aspx.cs" Inherits="StudentVibes.Evaluation" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
        <script>
            $(document).ready(function () {
                /*! Fades in page on load */
                $('body').css('display', 'none');
                $('body').fadeIn(800);
            });
        </script>
    <meta name="description" content="Receive an online quote to see how much you may receive
        for a pre-owned item before making your way to Student Vibes.">
    <meta name="author" content="Student Vibes">
    <h3>Get rid of a textbook for money!</h3>
    <address>
        Firstly, please enter your name:<br />
        <asp:TextBox runat="server" ID="CustName" type="text" class="form-control" style="margin-top: 3px; width: 300px" />    
    </address>
    <address>
        Enter your e-mail address:<br />
        <asp:TextBox runat="server" ID="EmailAddress" type="text" class="form-control" style="margin-top: 3px; width: 300px" />    
    </address>
    <address>
        Enter the name of the item:<br />
        <asp:TextBox runat="server" ID="ItemName" type="text" class="form-control" style="margin-top: 3px; width: 300px" />    
    </address>
    <address>
        Enter the Barcode number of the textbook:<br />
        <asp:TextBox runat="server" ID="Barcode" type="text" placeholder="e.g. 9780733426094" class="form-control" style="margin-top: 4px; width: 300px; height: 30px;" />
        </address>
    <address>
        Give the amount the item was purchased for:<br />
        <asp:TextBox runat="server" ID="ItemAmount" type="text" placeholder="e.g. 350" class="form-control" style="margin-top: 4px; width: 300px; height: 30px;" />
    </address>
    <address>
        Duration of item use (months):<br />
        <asp:TextBox runat="server" ID="Duration" type="text" placeholder="e.g. 4" class="form-control" style="margin-top: 4px; width: 300px; height: 30px;" />
    </address>
    <address>
        State the condition of the item:<br />
        <asp:DropDownList ID="Condition" runat="server" Height="30px">
            <asp:ListItem>Excellent</asp:ListItem>
            <asp:ListItem>Good</asp:ListItem>
            <asp:ListItem>Average</asp:ListItem>
            <asp:ListItem>Poor</asp:ListItem>
        </asp:DropDownList>
        </address>

        <address>
        <asp:CheckBox ID="CheckBox" runat="server" onchange="goFurther();" />
            <script type="text/javascript">
                // Popup window code
                function newPopup(url) {
                    popupWindow = window.open(url, 'popUpWindow', 'height=800,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
                }
            </script>
        <a href="JavaScript:newPopup('TermsAndConditions.aspx');">Accept Terms & Conditions</a><br>
        </address>

        <address>
            <script type="text/javascript">
                function goFurther() {
                    if (document.getElementById('<%=CheckBox.ClientID%>').checked == true)
                        document.getElementById('<%=SubmitInfo.ClientID%>').disabled = false;
                    else
                        document.getElementById('<%=SubmitInfo.ClientID%>').disabled = true;
                }
            </script>
        <asp:Button ID="SubmitInfo" runat="server" CssClass="btn btn-default" OnClick="SubmitInfo_Click" Text="Submit" Enabled="false" />
                <script type="text/javascript">
                    function custDetails(ref, price) {
                        confirm('Hi there ' + document.getElementById('<%=CustName.ClientID%>').value + '!\n'
                        + 'Your reference number is: ' + ref + '.\n'
                        + 'We could sell your book for around: R' + price + '\n'
                        + 'Pop into the store and we will give you a proper evaluation :)');
                        window.location = "Default.aspx";
                    }
                </script>
    </address>
</asp:Content>