﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace StudentVibes.Models
{
    public class WishList
    {
        public int WishListID { get; set; }

        public int CustomerID { get; set; }
        public int Quantity { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
    }
}