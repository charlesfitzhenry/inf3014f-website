﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace StudentVibes.Models
{
    // Changed from DropCreateDatabaseIfModelChanges to DropCreateDatabaseAlways
    public class ProductDatabaseInitializer : DropCreateDatabaseAlways<ProductContext>
    {
        protected override void Seed(ProductContext context)
        {
            GetCategories().ForEach(c => context.Categories.Add(c));
            GetProducts().ForEach(p => context.Products.Add(p));
            GetStates().ForEach(s => context.States.Add(s));
        }
        private static List<State> GetStates()
        {
            var states = new List<State> { 
                new State 
                { 
                    StateID = 1, 
                    StateName = "New" 
                }, 
                new State 
                { 
                    StateID = 2, 
                    StateName = "Used" 
                }, 
        };
            return states;
        }
        // Changed the product categories
        private static List<Category> GetCategories()
        {
            var categories = new List<Category> { 
                new Category 
                { 
                    CategoryID = 1, 
                    CategoryName = "Textbooks" 
                }, 
                new Category 
                { 
                    CategoryID = 2, 
                    CategoryName = "Gadgets"
                },
            };
            return categories;
        }
        private static List<Product> GetProducts()
        {
            var products = new List<Product> { 
                new Product 
                { 
                    ProductID = 1, 
                    ProductName = "Product1 its a textbook-new", 
                    Description = "This is product number one and it is new",
                    Author = "Author 1",
                    Isbn = "1781826374956",
                    Year = 1994,
                    ImagePath="carconvert.png", 
                    UnitPrice = 22.50, 
                    CategoryID = 1 ,
                    StateID = 1
                }, 
                new Product 
                { 
                    ProductID = 2, 
                    ProductName = "Product2 its a gadget-new", 
                    Description = "This is product number two and it is new", 
                    ImagePath="carearly.png", 
                    UnitPrice = 15.95, 
                    CategoryID = 2,
                    StateID = 1,
                    Brand = "Samsung"
                }, 
                new Product 
                { 
                    ProductID = 3, 
                    ProductName = "Product3 its a textbook-old", 
                    Description = "This is product number Three and it is old", 
                    Author = "Author 2",
                    Isbn = "1781830274956",
                    Year = 2001,
                    ImagePath="carfast.png", 
                    UnitPrice = 32.99, 
                    CategoryID = 1,
                    StateID = 2
                }, 
                new Product 
                { 
                    ProductID = 4, 
                    ProductName = "Product4 its a gadget- old",
                    Description = "This is product number four and it is old", 
                    ImagePath="carfaster.png", 
                    UnitPrice = 8.95, 
                    CategoryID = 2,
                    StateID = 2,
                    Brand = "Nokia"
                }
                /*new Product 
                { 
                    ProductID = 5, 
                    ProductName = "Old Style Racer", 
                    Description = "This old style racer can fly (with user assistance). Gravity controls flight duration."
                    + "No batteries required.", 
                    ImagePath="carracer.png", 
                    UnitPrice = 34.95, 
                    CategoryID = 1 ,
                    StateID = 1
                }, 
                new Product 
                { 
                    ProductID = 6, 
                    ProductName = "Ace Plane", 
                    Description = "Authentic airplane toy. Features realistic color and details.", 
                    ImagePath="planeace.png", 
                    UnitPrice = 95.00, 
                    CategoryID = 2 ,
                    StateID = 2
                }, 
                new Product 
                { 
                    ProductID = 7, 
                    ProductName = "Glider", 
                    Description = "This fun glider is made from real balsa wood. Some assembly required.", 
                    ImagePath="planeglider.png", 
                    UnitPrice = 4.95, 
                    CategoryID = 2 ,
                    StateID = 2
                }, 
                new Product 
                { 
                    ProductID = 8, 
                    ProductName = "Paper Plane", 
                    Description = "This paper plane is like no other paper plane. Some folding required.", 
                    ImagePath="planepaper.png", 
                    UnitPrice = 2.95, 
                    CategoryID = 2 ,
                    StateID = 1
                }, 
                new Product 
                { 
                    ProductID = 9, 
                    ProductName = "Propeller Plane", 
                    Description = "Rubber band powered plane features two wheels.", 
                    ImagePath="planeprop.png", 
                    UnitPrice = 32.95, 
                    CategoryID = 2,
                    StateID = 2
                },
       new Product 
                { 
                    ProductID = 10, 
                    ProductName = "Computer Science", 
                    Isbn = 494945645,
                    Year = 1999,
                    Description = "This is a textbook used to study Computer Science",
                    Author = "Charles",
                    ImagePath="calculus7thedition.png", 
                    UnitPrice = 120.50, 
                    CategoryID = 1,
                    StateID = 1
                }, 
                new Product 
                { 
                    ProductID = 12, 
                    ProductName = "Computer Science", 
                    Isbn = 46545645,
                    Year = 2000,
                    Description = "This is a textbook used to study Computer Science", 
                    Author = "Adi",
                    ImagePath="computerscience2.jpg", 
                    UnitPrice = 122.50, 
                    CategoryID = 1,
                    StateID = 2
                }, 
                new Product 
                { 
                    ProductID = 13, 
                    ProductName = "Computer Science", 
                    Isbn = 594945644,
                    Year = 2004,
                    Description = "This is a textbook used to study Computer Science",
                    Author = "Louba",
                    ImagePath="computerscience.png", 
                    UnitPrice = 222.50, 
                    CategoryID = 1,
                    StateID = 1
                },
                new Product 
                { 
                    ProductID = 14, 
                    ProductName = "Iphone 5s", 
                    Description = "The coolest phone ever!", 
                    ImagePath="iphone.jpg", 
                    UnitPrice = 3000.00, 
                    CategoryID = 2,
                    StateID = 1
                },
                new Product 
                { 
                    ProductID = 15, 
                    ProductName = "Nokia Lumia", 
                    Description = "Not such a cool phone as the Iphone", 
                    ImagePath="nokia.jpg", 
                    UnitPrice = 22.50, 
                    CategoryID = 2,
                    StateID = 2
                } */
            };
            return products;
        }
    }
}