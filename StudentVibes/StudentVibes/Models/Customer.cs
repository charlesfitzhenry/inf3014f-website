﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace StudentVibes.Models
{
    public class Customer
    {
        [ScaffoldColumn(false)]
        public string CustomerID { get; set; }
        public string Username { get; set; }

        [Required(ErrorMessage = "First Name is required")]
        [DisplayName("First Name")]
        [StringLength(160)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name is required")]
        [DisplayName("Last Name")]
        [StringLength(160)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Gender is required")]
        [DisplayName("Gender")]
        [StringLength(10)]
        public string Gender { get; set; }

        [Required, StringLength(100), Display(Name = "DoB")]
        public string DoB { get; set; }

        [Required(ErrorMessage = "Email Address is required")]
        [DisplayName("Email Address")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email is is not valid.")]
        
        [Display(Name = "Total Spent")]
        public double? TotalSpent { get; set; }

        public int? OrderID { get; set; }

        public virtual Order Order { get; set; }
    }
}