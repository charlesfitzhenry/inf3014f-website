﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace StudentVibes.Models
{
    public class State
    {
        [ScaffoldColumn(false)]
        public int StateID { get; set; }
        [Required, StringLength(100), Display(Name = "Name")]
        public string StateName { get; set; }

        /*[Display(Name = "Product Description")]
        public string Description { get; set; }*/
        public virtual Category Category { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}