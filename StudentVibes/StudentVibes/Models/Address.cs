﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel;

namespace StudentVibes.Models
{
    public class Address
    {
        public int AddressID { get; set; }

        [Required(ErrorMessage = "Street NUmber is required")]
        [StringLength(70)]
        public string StreetNumber { get; set; }
       
        [Required(ErrorMessage = "Street Name is required")]
        [StringLength(70)]
        public string StreetName { get; set; }
        [Required(ErrorMessage = "City is required")]
        [StringLength(40)]
        public string City { get; set; }
        [Required(ErrorMessage = "Province is required")]
        [StringLength(40)]
        public string Province { get; set; }
        [Required(ErrorMessage = "Zip Code is required")]
        [DisplayName("Postal Code")]
        [StringLength(10)]
        public string ZipCode { get; set; }
        [Required(ErrorMessage = "Country is required")]
        [StringLength(40)]
        public string Country { get; set; }

        public string CustomerID { get; set; }

    }
}