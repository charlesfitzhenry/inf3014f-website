﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using StudentVibes.Logic;
using System.Data;
using System.Data.SqlClient;

namespace StudentVibes
{
    public partial class Evaluation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void SubmitInfo_Click(object sender, EventArgs e)
        {
            string name = CustName.Text;
            Random rnd = new Random();
            string reference = "";
            double price = double.Parse(ItemAmount.Text.ToString()) *0.70;

            for (int i = 0; i < 10; i++)
            {
                reference += rnd.Next(0,10);
            }

            SqlConnection connection = new SqlConnection("Data Source=(LocalDB)\\v11.0;AttachDbFilename=|DataDirectory|\\StudentVibes.mdf;Integrated Security=True");
            connection.Open();
            using (SqlCommand cmd = new SqlCommand("INSERT INTO Evaluation (Reference, Email, ItemName, Barcode, Price, "
                + "Duration) VALUES (@Reference, @Email, @ItemName, @Barcode, @Price, @Duration)", connection))
            {
                cmd.Parameters.AddWithValue("@Reference", reference);
                cmd.Parameters.AddWithValue("@Email", EmailAddress.Text);
                cmd.Parameters.AddWithValue("@ItemName", ItemName.Text);
                cmd.Parameters.AddWithValue("@Barcode", Barcode.Text);
                cmd.Parameters.AddWithValue("@Price", ItemAmount.Text);
                cmd.Parameters.AddWithValue("@Duration", Duration.Text);
                cmd.ExecuteNonQuery();
            }
            connection.Close();

            ScriptManager.RegisterStartupScript(this, typeof(string), "confirm",
                "custDetails(" + reference + ", " + price + ");", true);
        }
    }
}

