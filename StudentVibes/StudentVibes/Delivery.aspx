﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Delivery.aspx.cs" Inherits="StudentVibes.Delivery" %>

<asp:Content ID="Delivery" ContentPlaceHolderID="MainContent" runat="server">

    <!-- this should go after your </body> -->
    <link rel="stylesheet" type="text/css" href="DatePicker/jquery.datetimepicker.css" />
    <script src="DatePicker/jquery.js"></script>
    <script src="DatePicker/jquery.datetimepicker.js"></script>


    <script>
        $().ready(function () {
            var radios = document.getElementsByName("addrs");
            var txtbox = document.getElementById("autocomplete");
            var addBtn = document.getElementById("MainContent_AddBtn");
            document.getElementById("orderSummary").style.display = "none";
            //txtbox.disabled = true;
            addBtn.disabled = true;




            for (var i = 0, max = radios.length; i < max; i++) {
                radios[i].onclick = function () {
                    if (this.id == "prevAddNewSelect") {
                        document.getElementById("newD").style.display = "";
                        document.getElementById("existD").style.display = "none"
                        txtbox.disabled = false;
                        addBtn.disabled = false;
                    }
                    else {

                        document.getElementById("newD").style.display = "none";
                        document.getElementById("existD").style.display = "";
                        txtbox.disabled = true;
                        addBtn.disabled = true;


                    };
                }
            }
        }
        );

        function displaySummary() {
            //OnClientClick = "/*displaySummary();*/"
            //Display order summary
            //alert("Display");
            //Call C# code to calculate distance

            var txtbox = document.getElementById("autocomplete");
            var addBtn = document.getElementById("MainContent_AddBtn");

            if (destinationA != "") {

                txtbox.disabled = true;

                document.getElementById("orderSummary").style.display = "";
            } else {
                alert("Please enter an address!");
            }

        }

    </script>

    <script>
        $(document).ready(function () {
            /*! Fades in page on load */
            $('body').css('display', 'none');
            $('body').fadeIn(800);
        });
    </script>


    <div id="deliveryAdd" style="margin-top: 20px">
        <h2>Delivery Address</h2>
        <br />
        <% 
            bool a = getStoredAddress();
            
        %>

        <div class="well">
            <%
                if (a)
                { %>
            <div id="existDiv">
                <span>
                    <label>
                        <input type="radio" id="prevAddSelect" name="addrs" />
                        Select Previous Address</label></span><br />
                <div id="existD" style="font-size: 14px; padding-left: 20px; padding-top: 10px">
                    <p>
                        <%: streetNumber + " " +streetName%><br />
                        <%: city %><br />
                        <%: province %><br />
                        <%: zipCode %><br />
                        <%: country %><br />
                    </p>
                </div>

            </div>
            <%} %>
            <div id="newDiv">
                <span>
                    <label>
                        <input type="radio" id="prevAddNewSelect" name="addrs" />
                        Add a New Address</label></span><br />

                <div id="newD" style="font-size: 14px; padding-left: 20px;">
                    <div id="locationField" style="margin-top: 20px">
                        <input id="autocomplete" placeholder="Enter your address"
                            onfocus="geolocate()" type="text">
                    </div>


                    <table id="address" style="margin-top: 20px">


                        <tr>
                            <td class="label">Street address</td>
                            <td class="slimField">
                                <input name="googletxt" class="field" id="street_number"
                                    disabled="true"></td>
                            <td class="wideField" colspan="2">
                                <input name="googletxt" class="field" id="route"
                                    disabled="true"></td>
                        </tr>
                        <tr>
                            <td class="label">City</td>
                            <td class="wideField" colspan="3">
                                <input name="googletxt" class="field" id="locality"
                                    disabled="true"></td>
                        </tr>
                        <tr>
                            <td class="label">Province</td>
                            <td class="slimField">
                                <input name="googletxt" class="field"
                                    id="administrative_area_level_1" disabled="true"></td>
                            <td class="label">Zip code</td>
                            <td class="wideField">
                                <input name="googletxt" class="field" id="postal_code"
                                    disabled="true"></td>
                        </tr>
                        <tr>
                            <td class="label">Country</td>
                            <td class="wideField" colspan="3">
                                <input name="googletxt" class="field"
                                    id="country" disabled="true"></td>
                        </tr>
                    </table>
                    <div id="content-pane">

                        <div id="outputDiv"></div>


                        <asp:HiddenField ID="dist" runat="server" />

                    </div>
                    <div id="map-canvas"></div>

                    <asp:UpdatePanel UpdateMode="Conditional" ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <br />
                            <asp:Button ID="AddBtn" OnClick="checkOut" Text="Add Address" runat="server" />
                            <br />
                            <asp:Label ID="Label1" runat="server"></asp:Label>
                            <!--<div class="progress progress-striped active" id="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 10%">
                                    <span class="sr-only">Calculating Distance</span>
                                </div>
                            </div>-->
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <div id="outOfRange">
    </div>
    <div>
        <h2>Order Summary</h2>
        <asp:UpdatePanel UpdateMode="Conditional" ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div>
                    <asp:Table ID="Table1" runat="server"
                        Style="width: 40%;"
                        GridLines="Both"
                        HorizontalAlign="Left">
                        <asp:TableRow HorizontalAlign="Center">
                            <asp:TableCell>
                <b>Description</b>
                            </asp:TableCell>
                            <asp:TableCell>
                <b>Price</b>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
               Order total
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Right">
                                <asp:Label ID="label5" runat="server"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Label ID="label2" runat="server"></asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Right">
                                <asp:Label ID="label3" runat="server"></asp:Label>

                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow BackColor="#c0c0c0">
                            <asp:TableCell>
               <b>Grand Total</b>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Right">
                                <b>
                                    <asp:Label ID="label4" runat="server"></asp:Label></b>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div style="clear: both; padding-top: 10px">
        <h2>Select Delivery Date & Time</h2>
        <input id="datetimepicker" type="text" placeholder="Select a date & time">
    </div>
    <script>


        jQuery('#datetimepicker').datetimepicker({
            onGenerate: function (ct) {

                $(this).find('.xdsoft_date.xdsoft_remove5days')

                .addClass('xdsoft_disabled');

            },

            lang: 'en',
            minDate: 0,
            remove5days: ['Sat', 'Sun'],
            step: 30,
            minTime: '08:00',

            maxTime: '20:30',

            format: 'd/m/Y H:i'

        });

    </script>


     <asp:ImageButton ID="CheckoutImageBtn2" runat="server" ImageUrl="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif"
                    Width="145" AlternateText="Check out with PayPal" OnClick="CheckoutBtn_Clicked"
                    BackColor="Transparent" BorderWidth="0" />
 
</asp:Content>
