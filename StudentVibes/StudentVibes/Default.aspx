﻿<%@ Page Title="Welcome" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="StudentVibes._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

  <h1><%: Title %>.</h1>
    <meta name="description" content="For all your textbooka and gadget needs, whether it's new or pre-owned, come
        and get them from Student Vibes.">
    <meta name="author" content="Student Vibes">
    <h2>Student Vibes is the place for all your textbook and gadget needs.</h2>
     <p class="lead">We're all about transportation toys. You can order class any of our toys today. Each toy listing has detailed information to help you choose the right toy.</p>        
      

     
    <div id="myCarousel" class="carousel slide">
            <ol class="carousel-indicators" style="list-style: none;">
                <li class="active" data-target="#myCarousel" data-slide-to="0"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Carousel items -->
            <div class="carousel-inner">
                <div class="active item">
                    <img src="/Catalog/Images/Thumbs/boatbig.png" alt="imagetext 1" />
                    <div class="carousel-caption">
                        <h4>Wide variety of textbooks and gadgets to choose from</h4>
                    </div>
                </div>
                <div class="item">
                    <img src="/Catalog/Images/Thumbs/busred.png" alt="imagetext 2" />
                    <div class="carousel-caption">
                        <h4>New and Pre-owned brands and titles</h4>
                    </div>
                </div>
                <div class="item">
                    <img src="/Catalog/Images/Thumbs/carracer.png" alt="imagetext 3" />
                    <div class="carousel-caption">
                        <h4>Come on over and get yours</h4>
                    </div>
                </div>
            </div>

            <!-- Carousel nav --> 
            <a class="carousel-control left" href="#myCarousel" data-slide="prev">‹</a> <a class="carousel-control right" href="#myCarousel" data-slide="next">›</a>
        </div>

        <!-- Call Carousel --> 
        <script type="text/javascript">
            (function ($) {
                $('.carousel').carousel({ interval: 5000, pause: 'hover' });
            })(jQuery);
        </script>

</asp:Content>