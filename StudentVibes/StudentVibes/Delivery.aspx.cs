﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Data.SqlClient;
using StudentVibes.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using StudentVibes.Logic;


namespace StudentVibes
{
    public partial class Delivery : System.Web.UI.Page
    {
        // Delivery variables
        public string streetNumber = "";
        public string streetName = "";
        public string city = "";
        public string province = "";
        public string zipCode = "";
        public string country = "";
        public double orderTotal = 0;
        public string deliverySummary = "";
        public double deliveryCost = 0;
        public bool orderProceed = false;
        public bool existingAddress = false;


        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["payment_amt"] != null)
            {
                orderTotal = Double.Parse(Session["payment_amt"].ToString());

                // HtmlGenericControl body = this.Master.FindControl("body") as HtmlGenericControl;

                // body.Attributes.Add("onLoad", "initialize();");



            }
            existingAddress = getStoredAddress();
        }
        public double getTotalCost()
        {

            return orderTotal + getDeliveryCost()[4];
        }
        public double[] getDeliveryCost()
        {
            double[] cost = new Double[5];
            double tmpDis = getDistance();

            //Free delivery within 15km
            if ((tmpDis < 15.0) && !(tmpDis < 0.0) && (tmpDis < 60.0))
            {
                //Check if free delivery applies order total > R250
                if (orderTotal > 250)
                {
                    switch (getLoyalty())
                    {

                        case "silver":
                            cost[0] = 1;// Loyalty position. 1= Silver, 2=Gold, 3=Platinum
                            cost[1] = 5;//Applicable delivery cost per km over free 15km
                            cost[2] = tmpDis; //total delivery distance
                            cost[3] = cost[2] - 15;//Distance exceeding 15km.
                            cost[4] = ((tmpDis - 15) * 5); //Applicable exceeding distance cost.
                            break;

                        case "gold":
                            cost[0] = 2;// Loyalty position. 1= Silver, 2=Gold, 3=Platinum
                            cost[1] = 3;//Applicable delivery cost per km over free 15km
                            cost[2] = tmpDis; //total delivery distance
                            cost[3] = cost[2] - 15;//Distance exceeding 15km.
                            cost[4] = ((tmpDis - 15) * 3); //Applicable exceeding distance cost.
                            break;

                        case "platinum":
                            cost[0] = 3;// Loyalty position. 1= Silver, 2=Gold, 3=Platinum
                            cost[1] = 0;//120 fixed price for further deliveries
                            cost[2] = tmpDis; //total delivery distance
                            cost[3] = 0;//Distance exceeding 15km.
                            cost[4] = 0; //Applicable exceeding distance cost.
                            break;


                    }

                    return cost;
                }
                else
                { //Cost < R250
                    switch (getLoyalty())
                    {

                        case "silver":
                            cost[0] = 1;// Loyalty position. 1= Silver, 2=Gold, 3=Platinum
                            cost[1] = 5;//Applicable delivery cost per km over free 15km
                            cost[2] = tmpDis; //total delivery distance
                            cost[3] = cost[2];//Distance exceeding 15km.
                            cost[4] = ((tmpDis) * 5); //Applicable exceeding distance cost.
                            break;
                        case "gold":
                            cost[0] = 2;// Loyalty position. 1= Silver, 2=Gold, 3=Platinum
                            cost[1] = 3;//Applicable delivery cost per km over free 15km
                            cost[2] = tmpDis; //total delivery distance
                            cost[3] = cost[2];//Distance exceeding 15km.
                            cost[4] = ((tmpDis) * 3); //Applicable exceeding distance cost.
                            break;

                        case "platinum":
                            cost[0] = 3;// Loyalty position. 1= Silver, 2=Gold, 3=Platinum
                            cost[1] = 1.50;//120 fixed price for further deliveries
                            cost[2] = tmpDis; //total delivery distance
                            cost[3] = cost[2];//Distance exceeding 15km.
                            cost[4] = cost[2] * cost[1]; //Applicable exceeding distance cost.
                            break;


                    }
                    return cost;

                }


            }
            else if (((tmpDis > 15.0) && !(tmpDis < 0.0) && (tmpDis < 60.0)))
            {
                //Check if free delivery applies order total > R250
                if (orderTotal > 250)
                {
                    switch (getLoyalty())
                    {

                        case "silver":
                            cost[0] = 1;// Loyalty position. 1= Silver, 2=Gold, 3=Platinum
                            cost[1] = 5;//Applicable delivery cost per km over free 15km
                            cost[2] = tmpDis; //total delivery distance
                            cost[3] = cost[2] - 15;//Distance exceeding 15km.
                            cost[4] = ((tmpDis - 15) * 5); //Applicable exceeding distance cost.
                            break;

                        case "gold":
                            cost[0] = 2;// Loyalty position. 1= Silver, 2=Gold, 3=Platinum
                            cost[1] = 3;//Applicable delivery cost per km over free 15km
                            cost[2] = tmpDis; //total delivery distance
                            cost[3] = cost[2] - 15;//Distance exceeding 15km.
                            cost[4] = ((tmpDis - 15) * 3); //Applicable exceeding distance cost.
                            break;

                        case "platinum":
                            cost[0] = 3;// Loyalty position. 1= Silver, 2=Gold, 3=Platinum
                            cost[1] = 0;//120 fixed price for further deliveries
                            cost[2] = tmpDis; //total delivery distance
                            cost[3] = 0;//Distance exceeding 15km.
                            cost[4] = 0; //Applicable exceeding distance cost.
                            break;


                    }

                    return cost;
                }
                else
                { //Cost < R250
                    switch (getLoyalty())
                    {

                        case "silver":
                            cost[0] = 1;// Loyalty position. 1= Silver, 2=Gold, 3=Platinum
                            cost[1] = 5;//Applicable delivery cost per km over free 15km
                            cost[2] = tmpDis; //total delivery distance
                            cost[3] = cost[2];//Distance exceeding 15km.
                            cost[4] = ((tmpDis) * 5); //Applicable exceeding distance cost.
                            break;
                        case "gold":
                            cost[0] = 2;// Loyalty position. 1= Silver, 2=Gold, 3=Platinum
                            cost[1] = 3;//Applicable delivery cost per km over free 15km
                            cost[2] = tmpDis; //total delivery distance
                            cost[3] = cost[2];//Distance exceeding 15km.
                            cost[4] = ((tmpDis) * 3); //Applicable exceeding distance cost.
                            break;

                        case "platinum":
                            cost[0] = 3;// Loyalty position. 1= Silver, 2=Gold, 3=Platinum
                            cost[1] = 1.50;//120 fixed price for further deliveries
                            cost[2] = tmpDis; //total delivery distance
                            cost[3] = cost[2];//Distance exceeding 15km.
                            cost[4] = cost[2] * cost[1]; //Applicable exceeding distance cost.
                            break;


                    }
                    return cost;

                }

            }




            else
            { //Distance > range
                cost[0] = -1;// Loyalty position. 1= Silver, 2=Gold, 3=Platinum
                cost[1] = 0;//Applicable delivery cost per km over free 15km
                cost[2] = tmpDis; //total delivery distance
                cost[3] = -1;//Distance exceeding 15km.
                cost[4] = -1; //Applicable exceeding distance cost.
                return cost;
            }



        }

        public bool getCustomerDetails()
        {
            UserManager man = new UserManager();
            var user = man.FindById(User.Identity.GetUserId());

            SqlConnection connection = new SqlConnection("Data Source=(LocalDB)\\v11.0;AttachDbFilename=|DataDirectory|\\StudentVibes.mdf;Integrated Security=True");
            //Query database and check if there are address
            connection.Open();
            SqlCommand cmd = new SqlCommand("SELECT StreetNumber, StreetName, City, Province, ZipCode, Country FROM Addresses WHERE CustomerID = '" + user.Id + "'", connection);

            // Check if there is an address already in the table for the customer
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {

                if (rdr.HasRows)
                {
                    rdr.Read();
                    //Values in db
                    existingAddress = true;
                    streetNumber = rdr["StreetNumber"].ToString();
                    streetName = rdr["StreetName"].ToString();
                    city = rdr["City"].ToString();
                    province = rdr["Province"].ToString();
                    zipCode = rdr["ZipCode"].ToString();
                    country = rdr["Country"].ToString();
                    return true;

                }
                else
                {
                    existingAddress = false;
                    return false;

                }
            }


        }




        private string getLoyalty()
        {
            return "silver";
        }
        public bool getStoredAddress()
        {

            //Query the DB with the current customer logged in and populate the address fields if it exists
            //Assign query to local variables 

            if (getCustomerDetails()) { return true; }
            else
            {
                return false;
            }

            //if(Query == success){
            //meaning there is an existing address
            /*string[] addrs = new string[7];

            addrs[0] = streetNumber;
            addrs[1] = streetName;
            addrs[2] = city;
            addrs[3] = province;
            addrs[4] = zipCode;
            addrs[5] = country;
            addrs[6] = "true";

            return addrs;*/


            //}else{
            //addrs[6] = "false";
            //}


        }

        public void setAddress()
        {
            SqlConnection connection = new SqlConnection("Data Source=(LocalDB)\\v11.0;AttachDbFilename=|DataDirectory|\\StudentVibes.mdf;Integrated Security=True");


            String[] tmp;
            tmp = Request.Params["googletxt"].Split(',');
            streetNumber = tmp[0];
            streetName = tmp[1];
            city = tmp[2];
            province = tmp[3];
            zipCode = tmp[4];
            country = tmp[5];

            UserManager man = new UserManager();
            var user = man.FindById(User.Identity.GetUserId());

            if (existingAddress)
            {
                //Update

                connection.Open();
                using (SqlCommand cmd = new SqlCommand("UPDATE Addresses SET StreetNumber = @StreetNumber" +
                ", StreetName = @StreetName, City = @City, Province = @Province" +
                ", ZipCode = @ZipCode, Country = @Country WHERE CustomerID='" + user.Id + "'", connection))
                {
                    cmd.Parameters.AddWithValue("@StreetNumber", streetNumber);
                    cmd.Parameters.AddWithValue("@StreetName", streetName);
                    cmd.Parameters.AddWithValue("@City", city);
                    cmd.Parameters.AddWithValue("@Province", province);
                    cmd.Parameters.AddWithValue("@ZipCode", zipCode);
                    cmd.Parameters.AddWithValue("@Country", country);


                    cmd.ExecuteNonQuery();
                }
                connection.Close();
            }
            else
            {
                //Insert new

                connection.Open();
                using (SqlCommand cmd = new SqlCommand("Insert INTO Addresses(StreetNumber, StreetName, City, Province, ZipCode, Country, CustomerID)" +
                    " values(@StreetNumber, @StreetName, @City, @Province, @ZipCode, @Country, @CustomerID)", connection))
                {
                    cmd.Parameters.AddWithValue("@StreetNumber", streetNumber);
                    cmd.Parameters.AddWithValue("@StreetName", streetName);
                    cmd.Parameters.AddWithValue("@City", city);
                    cmd.Parameters.AddWithValue("@Province", province);
                    cmd.Parameters.AddWithValue("@ZipCode", zipCode);
                    cmd.Parameters.AddWithValue("@Country", country);
                    cmd.Parameters.AddWithValue("@CustomerID", user.Id);



                    cmd.ExecuteNonQuery();
                }
                connection.Close();

            }




        }


        protected void CheckoutBtn_Clicked(object sender, ImageClickEventArgs e)
        {
            using (ShoppingCartActions usersShoppingCart = new ShoppingCartActions())
            {
                Session["payment_amt"] = usersShoppingCart.GetTotal();
            }
            Response.Redirect("Checkout/CheckoutStart.aspx");
        }


        public double getDistance()
        {
            while (dist.Value == null)
            {
                Thread.Sleep(2000);

            }

            string distance = dist.Value;
            if (dist.Value != "")
            {
                if (dist.Value.Contains(","))
                {
                    distance = distance.Replace(",", ".");
                }

                return Double.Parse(distance);

            }

            return -1;

        }
        protected void checkOut(object sender, EventArgs e)
        {

            setAddress();
            //ONLY ALLOW TO PROCEED IF LOGGED IN

            //PROCEED TO PAYPAL
            // Response.Redirect(dist.Value);
            double ds = getDistance();
            Double[] dl = getDeliveryCost();

            if (ds != -1)
            {
                //Delivery in range
                if (ds < 15)
                {
                    //Label under the add address button
                    Label1.Text = "You are " + dist.Value + "km from our warehouse";
                    //Labels in the order summary table
                    label2.Text = "Free Delivery @ " + ds + "km";
                    deliveryCost = 0;
                    label3.Text = "R" + deliveryCost;
                    label5.Text = "R" + orderTotal.ToString();
                    label4.Text = "R" + (orderTotal + deliveryCost);
                    orderProceed = true;
                    UpdatePanel2.Update();
                }
                else
                {   // Delivery out of all ranges
                    if (ds > 60)
                    {

                        //Label under the add address button
                        Label1.Text = "You are " + dist.Value + "km from our warehouse";
                        //Labels in the order summary table
                        label2.Text = "Delivery";
                        //deliveryCost = dl[4];
                        label3.Text = "OUTSIDE DELIVERY AREA";
                        label5.Text = orderTotal.ToString();
                        label4.Text = "R" + orderTotal;
                        orderProceed = false;
                        UpdatePanel2.Update();
                    }
                    else
                    {
                        //Check loyalty
                        string l = "";
                        if (dl[0] == 1)
                        {
                            l = "Silver";

                            //Label under the add address button
                            Label1.Text = "You are " + dist.Value + "km from our warehouse";
                            //Labels in the order summary table
                            label2.Text = (ds - 15) + "km (" + l + " Member) Delivery @ R" + dl[1] + "/km";
                            deliveryCost = dl[4];
                            label3.Text = "R" + deliveryCost;
                            label5.Text = "R" + orderTotal.ToString();
                            label4.Text = "R" + (orderTotal + deliveryCost);
                            orderProceed = true;
                            UpdatePanel2.Update();

                        }
                        else if (dl[0] == 2)
                        {
                            l = "Gold";

                            //Label under the add address button
                            Label1.Text = "You are " + dist.Value + "km from our warehouse";
                            //Labels in the order summary table
                            label2.Text = (ds - 15) + "km (" + l + " Member) Delivery @ R" + dl[1] + "/km";
                            deliveryCost = dl[4];
                            label3.Text = "R" + deliveryCost;
                            label5.Text = "R" + orderTotal.ToString();
                            label4.Text = "R" + (orderTotal + deliveryCost);
                            orderProceed = true;
                            UpdatePanel2.Update();
                        }
                        else if (dl[0] == 3)
                        {
                            l = "Platinum";

                            //Label under the add address button
                            Label1.Text = "You are " + dist.Value + "km from our warehouse";
                            //Labels in the order summary table
                            label2.Text = (ds - 15) + "km (" + l + " Member) Delivery @ R" + dl[1] + "/km";
                            deliveryCost = dl[4];
                            label3.Text = "R" + deliveryCost;
                            label5.Text = "R" + orderTotal.ToString();
                            label4.Text = "R" + (orderTotal + deliveryCost);
                            orderProceed = true;
                            UpdatePanel2.Update();
                        }


                    }
                }/*
                  ProductContext _db = new ProductContext();

                //Add delivery cost to cart

                  // Retrieve the product from the database. 
                ShoppingCartActions s = new ShoppingCartActions();
            string ShoppingCartId = s.GetCartId();
                // Create a new cart item if no cart item exists. 
                CartItem cartItem = new StudentVibes.Models.CartItem()
{
    ItemId = Guid.NewGuid().ToString(),
    ProductId = 1,
    CartId = ShoppingCartId,
    Product = _db.Products.SingleOrDefault(

        p => p.ProductID == id),
    Quantity = 1,
    DateCreated = DateTime.Now
};
                _db.ShoppingCartItems.Add(cartItem);
            
            else
            {// If the item does exist in the cart, // then add one to the quantity. 
                cartItem.Quantity++;
            }
            _db.SaveChanges();

        */

                Session["streetNumber"] = streetNumber;
                Session["streetName"] = streetName;
                Session["city"] = city;
                Session["province"] = province;
                Session["zipCode"] = zipCode;
                Session["country"] = country;



            }


        }
    }
}