﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using StudentVibes.Models;
using System.Web.ModelBinding;
using System.Web.Routing;


namespace StudentVibes
{
    public partial class ProductList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public IQueryable<Product> GetProducts(/*[QueryString("id")] int? categoryId,[QueryString("id")] int? stateId,*/ [RouteData] string categoryName, [RouteData] string itemStatus)
        {
            var _db = new StudentVibes.Models.ProductContext();
            IQueryable<Product> query = _db.Products;
            /* if (categoryId.HasValue && categoryId > 0)
             {
                 query = query.Where(p => p.CategoryID == categoryId);
             }*/

            /* if (stateId.HasValue && stateId > 0)
             {
                 query = query.Where(p => p.StateID == stateId);
             }*/

            if (!String.IsNullOrEmpty(categoryName))
            {
                query = query.Where(p => String.Compare(p.Category.CategoryName, categoryName) == 0);
            }

            if (!String.IsNullOrEmpty(itemStatus))
            {
                query = query.Where(p => String.Compare(p.State.StateName, itemStatus) == 0);
            }

            return query;
        }

    }
}