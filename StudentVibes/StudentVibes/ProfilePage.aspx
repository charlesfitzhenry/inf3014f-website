﻿<%@ Page Title="ProfilePage" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Evaluation.aspx.cs" Inherits="StudentVibes.Evaluation" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <meta name="description" content="A great list of all textbooks and gadgets available
        from Student Vibes.">
    <meta name="author" content="Student Vibes">
    <script>
        $(document).ready(function () {
            /*! Fades in page on load */
            $('body').css('display', 'none');
            $('body').fadeIn(800);
        });
    </script>
    <h2><%: Title %>Profile</h2>
    <h3>Happy with your settings? Or nah?</h3>
    <h4>Account Settings</h4>
    <address>
        E-mail:<br />
        <input type="text" class="form-control" style="margin-top: 3px; width: 300px; height: 28px;">
    </address>
    <address>
        Password:<br />
        <input type="password" class="form-control" style="margin-top: 4px; width: 300px; height: 28px;">
    </address>

    <h4>Profile Settings</h4>
    <address>
        Name:<br />
        <input type="text" class="form-control" style="margin-top: 4px; width: 300px; height: 30px;">
    </address>
    <address>
        ID Number:<br />
        <input type="text" class="form-control" style="margin-top: 4px; width: 300px; height: 30px;">
    </address>
    <address>
        Location:<br />
        <input type="text" class="form-control" style="margin-top: 4px; width: 300px; height: 30px;">
    </address>

    <address>
        <asp:Button ID="Save" class="form-control" runat="server" Text="Save Changes" Width="138px" />
    </address>
</asp:Content>
